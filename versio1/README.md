@font-face {
    font-family: fight;
    src: url(../font/Fight\ Night.otf);
}
@font-face {
    font-family: precursive;
    src: url(../font/Precursive_1_FREE.otf);
}
body{
    background-color: rgb(46, 20, 47);
    display: flex;
    flex-direction: column;
    margin: 0%;
}

.header{
    font-family: fight;
    padding: 60px;
    text-align: left;
    background-image: url("../img/orig_672676.jpg");
    color:rgb(84, 209, 240);
    font-size: 50px;
}
.menubtn{
    background-color: cyan;
    color: black;
    padding: 16px;
    font-size: 25px;
    border: 2px;
    font-family: fight;
}
.menubtn a{
    text-decoration:none;
    color: black;
}
.dropdown{
    position:relative;
    display: inline-block;
}
.menu-content{
    display: none;
    position: absolute;
    background-color: #1f7792;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    font-size: 50%;
}
.menu-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.menu-content a:hover {background-color: #ddd;}
.dropdown:hover .menu-content {display: block;}
.dropdown:hover .menubtn {background-color: #1a7b99;}

.main-content{
    flex-direction: column;
    background-color: #ca89f5;
    display: row;
    margin: 1%;
}
h2{
    text-align: center;
    font-family: fight;
}
p{
    font-family: precursive;
    text-align: justify;
}
h3{
    text-align: center;
    font-family: fight;
}
.imgactors{
    display: block;
    margin-left: auto;
    margin-right: auto;
    align-content: center;
    height: 15%;
    width: 15%;
}
.taula{
    margin-left: auto;
    margin-right: auto;
}
.taula table, th, td{
    border: 1px solid black;
    background: #ddd;
}
.img{
    display: row;
    height: 20%;
    width: 20%;


}
.video{
    align-content: center;
}
.footer{
    color: white;
    display: flex;
    flex-direction: row;
}
.footer p{
    margin: 1%;
}